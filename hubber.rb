#!/usr/bin/env ruby

require 'fileutils'
require 'json'
require 'net/http'
require 'uri'

module HTTPSRequest
  extend self

  # make json post request.
  def postJson(url, hash_json)
    uri = URI(url)
    req = Net::HTTP::Post.new(uri.request_uri)

    req.body = hash_json.to_json
    req['Content-Type'] = 'application/json'
    req['Accept'] = 'application/json'
    req['Referer'] = 'https://api.mghubcdn.com/graphql'
    req['Origin'] = 'https://mangahub.io'
    # felt like adding this. not much of a point though.
    req['Dnt'] = '1'
    req['Connection'] = 'keep-alive'
    req['Te'] = 'Trailers'

    # make request and return the body of the response.
    https(uri).request(req).body
  end

  # make get request.
  def get(url)
    uri = URI(url)

    https(uri).request(Net::HTTP::Get.new(uri.request_uri)).body
  end

  private

  # use https.
  def https(uri)
    Net::HTTP.new(uri.host, uri.port).tap do |http|
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
  end
end

module GetSeries
  extend self

  def get(series)
    FileUtils.mkdir(series) unless File.exist?(series)
    FileUtils.cd(series) do
      puts "get series: #{series}"
      chapIDs(series).each { |id| getChapter(series, id) }
    end
  end

  private

  # all json requests go here.
  REQUEST_URL = 'https://api.mghubcdn.com/graphql'

  # get the mangaID of a series.
  # chaptersByManga queries require the mangaID for whatever reason.
  def mangaID(series)
    # getting the mangaID isn't very straightforward.
    # however, it seems to be included in queries for individual chapters.
    # so, we can just query for chapter 1 to get it.
    # interestingly, this seems to work even if there is no chapter 1.
    q = {"query":"{chapter(x:m01,slug:\"#{series}\",number:1){mangaID}}"}
    HTTPSRequest.postJson(REQUEST_URL, q)[/\d+/]
  end

  # get the numbers of all chapters in a series.
  # there are often chapters ending in decimals, so we can't just take a range.
  def chapIDs(series)
    query = {"query":"{chaptersByManga(mangaID:#{mangaID(series)}){number}}"}
    HTTPSRequest.postJson(REQUEST_URL, query).scan(/\d+\.?\d*/)
  end

  # get the url paths to each page in a chapter.
  def pagePaths(series, chapID)
    q = {"query":"{chapter(x:m01,slug:\"#{series}\",number:#{chapID}){pages}}"}
    HTTPSRequest.postJson(REQUEST_URL, q).scan(/[^"]+\/\d+\.?\d*\/\d+\.\w+/)
  end

  # download individual page.
  def page(dir, page)
    # pages are named page#.jpg
    filename = page[/\d+\.\w+$/]
    pageURL = "https://img.mghubcdn.com/file/imghub/#{page}"

    unless File.exist?(filename)
      puts "get: #{dir}/#{filename}"
      File.write(filename, HTTPSRequest.get(pageURL))
    end
  end

  # don't want to flood the servers with a lot of connections.
  # feel free to change this. just please be nice.
  THREAD_LIMIT = 8

  # download all pages in a chapter.
  def getChapter(series, chapID)
    # name for the chapter's directory.
    dir = "chapter #{chapID}"
    pages = pagePaths(series, chapID)

    # don't download if we couldn't find any pages.
    # this is usually due to malformed requests or invalid chapters.
    unless pages.empty?
      FileUtils.mkdir(dir) unless File.exist?(dir)
      FileUtils.cd(dir) do
        threads = []

        pages.each do |p|
          # stolen from stackoverflow. works pretty well.
          until threads.map { |t| t.alive? }.count(true) < THREAD_LIMIT do
            sleep 2
          end

          threads << Thread.new { page(dir, p[0]) }
        end

        # wait for everything to finish.
        threads.each { |t| t.join }
      end
    end
  end
end

# can pass multiple series as args.
ARGV.each do |url|
  seriesURI = URI(url)

  if seriesURI.host == 'mangahub.io'
    series = /^\/manga\/([\w-]+)(\/.*)?/.match(seriesURI.path)[1]
    GetSeries.get(series) unless series.empty?
  else
    puts "invalid domain: #{seriesURI.host}"
    # probably going to expand on this poc at some point.
    puts "only mangahub.io is supported."
  end
end
